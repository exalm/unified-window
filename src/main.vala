/* main.vala
 *
 * Copyright 2020 Alexander Mikhaylenko
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

int main (string[] args) {
    typeof (SingleWindow.TitleBar).ensure ();

    var app = new Gtk.Application ("org.example.Window", ApplicationFlags.FLAGS_NONE);

    app.startup.connect (() => {
        var screen = Gdk.Screen.get_default ();
        var provider = new Gtk.CssProvider ();
        provider.load_from_resource ("/org/example/Window/style.css");
        Gtk.StyleContext.add_provider_for_screen (screen, provider, 600);
    });

    app.activate.connect (() => {
        var win = app.active_window;
        if (win == null)
            win = new SingleWindow.Window (app);

        win.present ();
    });

    return app.run (args);
}
